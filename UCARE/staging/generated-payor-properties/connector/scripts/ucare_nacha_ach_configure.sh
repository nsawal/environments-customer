#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2

PROPERTY_FILE=./ucare_env.properties

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

echo ""
echo "********** UCARE SH : ucare_nacha_ach_configure.sh start on ${HOST_NAME} **********"


    echo "Start configuring Nacha ACH job"

        positivepayJsonConfigFile=$HE_DIR/ucare-nachasub/resources/config/nachasub-job-config*.json
        positivepayJsonRouteFile=$HE_DIR/ucare-nachasub/resources/config/nachasub-route-config_bank1.json
        positivepayJsonRouteFile1=$HE_DIR/ucare-nachasub/resources/config/nachasub-route-config_bank2.json
        positivepayJsonRouteFile2=$HE_DIR/ucare-nachasub/resources/config/nachasub-route-config_bank3.json
        positivepayJsonRouteFile3=$HE_DIR/ucare-nachasub/resources/config/nachasub-route-config_bank4.json

        chmod -R 755 $positivepayJsonConfigFile
        chmod -R 755 $positivepayJsonRouteFile
        chmod -R 755 $positivepayJsonRouteFile1
        chmod -R 755 $positivepayJsonRouteFile2
        chmod -R 755 $positivepayJsonRouteFile3

        sed -i -e "s|#HE_DIR#|${HE_DIR}|g" $positivepayJsonConfigFile

        extractEndTime=$(date +"%Y-%m-%dT%T.%3NEDT")
        extractStartEndTime=$(date +"%Y-%m-%dT%T.%3NEDT" -d "1 month ago")
        echo "extractStartEndTime: ${extractStartEndTime}"

        sed -i -e "s|SSSS-MM-DDTHH:MM:SS.SSSZEDT|${extractEndTime}|g" $positivepayJsonConfigFile
        sed -i -e "s|EEEE-MM-DDTHH:MM:SS.SSSZEDT|${extractEndTime}|g" $positivepayJsonConfigFile

        bankInfoInputDir=$(grep "bankInfoInputDir" $HE_DIR/etc/com.healthedge.customer.ucare.nacha.sub.extract.cfg  | sed -e "s/bankInfoInputDir=//g")
        cp $HE_DIR/etc/BANK*.cfg ${bankInfoInputDir}/


        extractOutputPath=$(grep "inputFolder" $HE_DIR/etc/com.healthedge.customer.ucare.nacha.sub.extract.cfg | sed -e "s/inputFolder=//g")
        echo "extractOutputPath: ${extractOutputPath}"

        sed -i -e "s|#EXTRACT_OUTPUT_PATH#|${extractOutputPath}|g" $positivepayJsonConfigFile

        extractConfigRequestDir=$(grep "extractConfigRequestDir" $HE_DIR/etc/com.healthedge.he.common.extract.configuration.cfg | sed -e "s/extractConfigRequestDir=//g")
        echo "extractConfigRequestDir: ${extractConfigRequestDir}"

        echo "Dropping Nacha ACH  job configuration json ${positivepayJsonConfigFile} to ${extractConfigRequestDir}"
        cp $positivepayJsonConfigFile $extractConfigRequestDir

    echo "End configuring Nacha ACH  job"


    echo "Start configuring Nacha ACH job trigger route"

        extractCron=$(getProperty positive_pay_extract_job_trigger_cron)
        echo "extractCron: ${extractCron}"

        sed -i -e "s|#EXTRACT_TIME#|${extractCron}|g" $positivepayJsonRouteFile
        sed -i -e "s|#EXTRACT_TIME#|${extractCron}|g" $positivepayJsonRouteFile1
		sed -i -e "s|#EXTRACT_TIME#|${extractCron}|g" $positivepayJsonRouteFile2
		sed -i -e "s|#EXTRACT_TIME#|${extractCron}|g" $positivepayJsonRouteFile3


        fileRequestBaseDir=$(grep "fileRequestBaseDir" $HE_DIR/etc/com.healthedge.he.common.scheduler.cfg | sed -e "s/fileRequestBaseDir=//g")
        stagingDir=$(grep "stagingDir" $HE_DIR/etc/com.healthedge.he.common.scheduler.cfg | sed -e "s/stagingDir=//g")
        triggerFileBaseDir=$(grep "triggerFileBaseDir" $HE_DIR/etc/com.healthedge.he.common.scheduler.cfg | sed -e "s/triggerFileBaseDir=//g")

        mkdir -p ${fileRequestBaseDir}
        mkdir -p ${stagingDir}
        mkdir -p ${triggerFileBaseDir}


        fileRequestBaseDir=${fileRequestBaseDir}request/cron

        echo "Dropping Nacha ACH job trigger creation json ${positivepayJsonRouteFile} to ${fileRequestBaseDir}"
        cp $positivepayJsonRouteFile $fileRequestBaseDir

sleep 1


         echo "Dropping Nacha ACH job trigger creation json ${positivepayJsonRouteFile1} to ${fileRequestBaseDir}"
        cp $positivepayJsonRouteFile1 $fileRequestBaseDir
        
sleep 1


         echo "Dropping Nacha ACH job trigger creation json ${positivepayJsonRouteFile2} to ${fileRequestBaseDir}"
        cp $positivepayJsonRouteFile2 $fileRequestBaseDir
        
        
sleep 1


         echo "Dropping Nacha ACH job trigger creation json ${positivepayJsonRouteFile3} to ${fileRequestBaseDir}"
        cp $positivepayJsonRouteFile3 $fileRequestBaseDir

    echo "End configuring Nacha ACH job trigger route"


echo "********** UCARE SH : ucare_nacha_ach_configure.sh end on ${HOST_NAME} **********"
echo ""

