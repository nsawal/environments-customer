nohup: ignoring input
!!! ERROR: The environment host directory generated-payor-properties//hosts does not exist
Description:
    This script upgrades the HealthRules software products.
Usage:
    he-upgrade.sh -e <environment_name> -p [payor|connector|connector-classic|payor-custom|care-admin|iway] [-f]
       -e: Environment name. The environment directory containing the installer properties files.
       -p: Products to upgrade. Specify which product to upgrade. Currently only one value of payor, connector, connector-classic, payor-custom, care-admin or iway is supported.
           If omitted, all products will be upgraded.
       -f: Force upgrade. Force the immediate upgrade of the product specified in the -p argument.
       -c: Customer name in perforce.  This option will force installer to use properties files from source control.
Examples:
    he-upgrade.sh -e uat -p connector
    (Upgrade connector in the "uat" environment)

    he-upgrade.sh -e qa
    (Upgrade all products in the "qa" environment)
0.00user 0.00system 0:00.01elapsed 55%CPU (0avgtext+0avgdata 1632maxresident)k
0inputs+0outputs (0major+2586minor)pagefaults 0swaps
